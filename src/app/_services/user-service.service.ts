import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpEvent, HttpEventType, HttpErrorResponse} from '@angular/common/http';
import {User} from 'src/app/models/User';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  userData : User;

  readonly APIURl = "https://localhost:44344/api/user/";

  constructor(private http:HttpClient) { }

  //GET USER DETAILS FOR ADMIN LOGIN
  getUserList(): Observable<User[]>
  {
    return this.http.get<User[]>(this.APIURl).pipe(catchError(this.handleError));
  }

  //GET USER DETAILS FOR ADMIN LOGIN
  getUser(email:string): Observable<User>
  {
    return this.http.get<User>(this.APIURl+email).pipe(catchError(this.handleError));
  }

  //REGISTER USER
  register(user:User):Observable<User>{
    return this.http.post<User>(this.APIURl,user).pipe(catchError(this.handleError));
  }

  handleError(err:HttpErrorResponse){
      return Observable.throw(err.error.text || "Server Error");
  }

}
