import { Component, OnInit } from '@angular/core';
import {UserServiceService} from '../../_services/user-service.service';
import { User } from 'src/app/models/User';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  public user:User = new User();
  constructor(public userService:UserServiceService) {}


  ngOnInit() {
    if(localStorage.getItem("jwt")!=null && localStorage.getItem("role")=="User"){
      this.userService.getUser(localStorage.getItem("email")).subscribe(
        data=>{
          this.user = data;
        },
        error=>{
          console.log(error);
        }
      )
    }
  }


}
