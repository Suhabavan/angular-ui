import { Component, OnInit } from '@angular/core';
import {UserServiceService} from '../../_services/user-service.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {
  private rowData;
  public manager:boolean=false;
  public user:boolean=false;
  constructor(private userservice:UserServiceService, private router:Router) {
    if(localStorage.getItem("jwt")!=null && localStorage.getItem("role")=="Manager"){
      this.manager=true;
      this.user=false;
    }
    else if(localStorage.getItem("jwt")!=null && localStorage.getItem("role")=="User"){
      this.user=true;
      this.manager=false;
    }
    else this.router.navigate(['/home']);
   }

  ngOnInit() {
    this.refreshDepList();
  }

  title = 'my-app';

  columnDefs = [
      { field: 'firstname',header:'FirstName' , sortable: true},
      { field: 'lastname',header:'LastName', sortable: true },
      { field: 'gender',header:'Gender' , sortable: true},
      { field: 'email',header:'Email' , sortable: true,filter: true},
      { field: 'mobileNumber',header:'Mobile Number', sortable: true,filter: true },
      { field: 'areaOfInterest',header:'Area of interest', sortable: true }
  ];


  refreshDepList(){
    this.userservice.getUserList().subscribe(data => {
       this.rowData = data;
    }
    );
  }

}
