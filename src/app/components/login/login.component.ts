import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {UserServiceService} from '../../_services/user-service.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {NgForm} from '@angular/forms';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    public userservice:UserServiceService,
    private router: Router,
    private snackBar:MatSnackBar,
    private http:HttpClient) { }

  ngOnInit() {}


  login(form:NgForm){
    const credentials = {
      'username': form.value.username,
      'password': form.value.password
    }
    this.http.post("https://localhost:44344/api/auth/login",credentials)
      .subscribe(
        response=>{
                    const token = (<any>response).token;
                    const role = (<any>response).role;
                    localStorage.setItem("jwt",token);
                    localStorage.setItem("role",role);
                    localStorage.setItem("email",form.value.username);
                    this.snackBar.open("Logged in successfully",'',{
                      duration:3000,
                      verticalPosition:'bottom'
                      });
                      this.router.navigate(['/home']);
                  },
        err=> {
                  this.snackBar.open(err.error,'',{
                    duration:4000,
                    verticalPosition:'bottom'
                    });
              }
        )
  }

}
