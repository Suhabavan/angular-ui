import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public manager:boolean=false;
  public user:boolean=false;
  public title:boolean=true;


  constructor() {}

  ngOnInit() {
    if(localStorage.getItem("jwt")!=null && localStorage.getItem("role")=="Manager"){
      this.manager=true;
      this.user=false;
      this.title=false;
    }
    else if(localStorage.getItem("jwt")!=null && localStorage.getItem("role")=="User"){
      this.user=true;
      this.manager=false;
      this.title=false;
    }
    else {
      this.user=false;
      this.manager=false;
      this.title=true;
    }
  }

}
