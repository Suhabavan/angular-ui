import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {UserServiceService} from '../../_services/user-service.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {User} from 'src/app/models/User';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  private user:User;
  registerForm: FormGroup;
  submitted = false;
  data:any;
  settings:any;


  constructor(
    public userservice:UserServiceService,
    private formBuilder: FormBuilder,
    private router: Router,
    private snackBar:MatSnackBar
    ) { }

  ngOnInit() {
    this.data = ["C++","Java","Nodejs",".Net"]; //Multiselect options for Area Of Interest
    this.settings = {
      singleSelection: false,
      allowSearchFilter: true,
      clearSearchFilter: true,
      maxHeight: 150,
      itemsShowLimit: 1
    };

    //Validation for Input fields
    this.registerForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(8)]],
      cpassword: ['', [Validators.required, Validators.minLength(8)]],
      gender: ['', [Validators.required]],
      mobileNumber: ['', [Validators.required]],
      areaOfInterest: ['', [Validators.required]],
  });
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
        return;
     }
    this.user = new User();
    this.user.firstname=this.registerForm.value.firstname;
    this.user.lastname=this.registerForm.value.lastname;
    this.user.gender=this.registerForm.value.gender;
    this.user.email=this.registerForm.value.email;
    this.user.password=this.registerForm.value.password;
    this.user.mobileNumber= this.registerForm.value.mobileNumber;
    this.user.areaOfInterest=this.registerForm.value.areaOfInterest.toString();

    if(this.user!=null)
    {
        this.userservice.register(this.user)
            .subscribe(
              data=>{
                this.snackBar.open("Registration successful. Please Login now",'',{
                    duration:3000,
                    verticalPosition:'bottom'
                });
                setTimeout(() => { this.router.navigate(['/login']); }, 3010); //wait 3.01 second and then load the login page
              },
              error=>{
                this.snackBar.open(error,'',{
                  duration:3000,
                  verticalPosition:'bottom'
                });
              }
            )
    }
  }
}
