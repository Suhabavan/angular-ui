import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'user-app';

  constructor(private router:Router){}

  loggedin(){
    return localStorage.getItem("jwt");
  }

  logout(){
    localStorage.removeItem("jwt");
    localStorage.removeItem("role");
    localStorage.removeItem("email");
    this.router.navigate(['/logout']);
  }

}
